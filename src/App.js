import React, { Component } from "react";
import parse from "url-parse";
import Form from "react-jsonschema-form";
import fields from "react-jsonschema-form-extras";
import { forEach, has, isEmpty, mapValues, merge, concat } from "lodash";
import Header from "./components/header";
import LoaderSpinner from "./components/loaderSpinner";
import * as api from "./api";
import "./App.css";

class App extends Component {
  targetHost = process.env.REACT_APP_GITLAB_URL;
  constructor() {
    super();
    this.state = {
      loading: true,
      error: null,
      schema: {},
      uiSchema: {},
      formData: {},
      project: null,
      branch: null,
      uiConfig: ".bod-ui.yml",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  fetchProjectForm(update = true) {
    const schema = {
      properties: {
        project: {
          title: "Project Name",
          type: "string",
        },
      },
      required: ["project"],
      title: "Build On Demand",
      type: "object",
    };

    const uiSchema = {
      project: {
        "ui:field": "asyncTypeahead",
        asyncTypeahead: {
          minLength: 0,
          url: `${this.targetHost}/api/v4/search`,
          search: (url, query) =>
            fetch(`${url}?search=${query}&scope=projects`, {
              headers: {
                Authorization: "Bearer " + window.gitlab_token,
                "Content-Type": "application/json",
              },
            })
              .then((response) => response.json())
              .then((json) => json),
          labelKey: "path_with_namespace",
          mapping: "path_with_namespace",
        },
      },
    };
    if (update) {
      this.setState({ loading: false, schema: schema, uiSchema: uiSchema });
    }
    return { schema, uiSchema };
  }
  fetchProjectBranchForm(project, update = true) {
    let { schema, uiSchema } = this.fetchProjectForm(false);
    schema.properties.project.default = project;
    schema.properties.branch = {
      title: "Branch Name",
      type: "string",
    };
    schema.required.push("branch");

    uiSchema.branch = {
      "ui:field": "asyncTypeahead",
      asyncTypeahead: {
        minLength: 0,
        url: `${this.targetHost}/api/v4/projects/${encodeURIComponent(
          project
        )}/repository/branches`,
        search: (url, query) =>
          fetch(`${url}?search=${query}`, {
            headers: {
              Authorization: "Bearer " + window.gitlab_token,
              "Content-Type": "application/json",
            },
          })
            .then((response) => {
              if (response.ok) return response.json();
              return Promise.resolve([]);
            })
            .then((json) => json),
        labelKey: "name",
        mapping: "name",
      },
    };
    if (update) {
      this.setState({
        loading: false,
        schema: schema,
        uiSchema: uiSchema,
        project: project,
      });
    }
    return { schema, uiSchema };
  }
  fetchForm(project, branch, uiConfig) {
    let { schema, uiSchema } = this.fetchProjectBranchForm(project, false);

    if (uiConfig) {
      if (
        !isEmpty(this.state.schema) &&
        this.state.schema.properties.hasOwnProperty("uiConfig")
      ) {
        schema.properties.uiConfig = this.state.schema.properties.uiConfig;
        schema.properties.uiConfig.default = uiConfig;
      }
    } else {
      uiConfig = ".bod-ui.yml";
    }

    api
      .fetchFormData(project, branch, uiConfig)
      .then((userSchemaDefinition) => {
        schema.properties.branch.default = branch;
        if (userSchemaDefinition.hasOwnProperty("required"))
          schema.required = concat(
            schema.required,
            userSchemaDefinition.required
          );
        if (userSchemaDefinition.hasOwnProperty("definitions"))
          schema.definitions = userSchemaDefinition.definitions;
        if (userSchemaDefinition.hasOwnProperty("dependencies"))
          schema.dependencies = userSchemaDefinition.dependencies;
        if (userSchemaDefinition.hasOwnProperty("title"))
          schema.title = userSchemaDefinition.title;
        if (userSchemaDefinition.hasOwnProperty("properties"))
          schema.properties = merge(
            schema.properties,
            userSchemaDefinition.properties
          );
        if (userSchemaDefinition.hasOwnProperty("ui"))
          uiSchema = merge(uiSchema, userSchemaDefinition.ui);

        // Remove query params not part of form schema
        const urlLocation = parse(window.location, true);
        const extraArgs = mapValues(urlLocation.query, (value, key) => {
          if (value) return { default: value };
        });
        forEach(extraArgs, (value, key) => {
          if (!has(schema.properties, key)) delete extraArgs[key];
        });

        schema = merge(schema, { properties: extraArgs });
        this.setState({
          loading: false,
          schema: schema,
          uiSchema: uiSchema,
          error: null,
          project: project,
          branch: branch,
          uiConfig: uiConfig,
        });
      })
      .catch((error) => {
        this.setState({
          loading: false,
          error: `Cannot fetch project form: ${error.statusText}`,
          project: project,
          branch: branch,
          uiConfig: uiConfig,
        });
      });
  }
  handleChange({ formData }) {
    if (isEmpty(formData)) return;

    const project = formData.project || null;
    const branch = formData.branch || null;
    const uiConfig = formData.uiConfig || null;

    if (this.state.project !== project) {
      if (project) {
        this.fetchProjectBranchForm(project);
      } else {
        this.fetchProjectForm();
      }
      return;
    }

    if (this.state.branch !== branch) {
      if (branch) {
        this.fetchForm(project, branch);
      } else {
        this.fetchProjectBranchForm(project);
      }
      return;
    }

    if (uiConfig && this.state.uiConfig !== uiConfig) {
      this.fetchForm(project, branch, uiConfig);
      return;
    }

    /* eslint no-eval: 0 */
    var uiSchema = { ...this.state.uiSchema };
    forEach(uiSchema, (value, key) => {
      // Evaluate all url in case a field is defined as AsyncTypeahead
      if (value.hasOwnProperty("asyncTypeahead")) {
        try {
          if (!value.asyncTypeahead.hasOwnProperty("evalUrl")) return;
          uiSchema[key].asyncTypeahead.url = eval(value.asyncTypeahead.evalUrl);
        } catch (e) {
          console.log(e);
        }
      }
    });
  }
  handleSubmit({ formData }) {
    api
      .triggerBuild(formData)
      .then((pipeline) => {
        window.open(pipeline.web_url);
      })
      .catch((error) => {
        this.setState({
          loading: false,
          error: `Error (${error.status}): ${error.statusText}`,
        });
      });
  }

  componentDidMount() {
    const urlLocation = parse(window.location, true);
    const project = urlLocation.query.project;
    const branch = urlLocation.query.branch;
    const uiConfig = urlLocation.query.uiConfig;

    if (project && branch) {
      this.fetchForm(project, branch, uiConfig);
    } else if (project) {
      this.fetchProjectBranchForm(project);
    } else {
      this.fetchProjectForm();
    }
  }
  render() {
    let form = null;
    let error = null;
    if (this.state.loading) {
      form = <LoaderSpinner loadingMessage="Fetching project form ..." />;
    }
    if (!isEmpty(this.state.schema)) {
      form = (
        <Form
          className="App-body container"
          fields={fields}
          schema={this.state.schema}
          uiSchema={this.state.uiSchema}
          onChange={this.handleChange}
          onSubmit={this.handleSubmit}
        >
          {this.state.error && (
            <div>
              <button className="hidden" disabled />
            </div>
          )}
        </Form>
      );
    }
    if (this.state.error) {
      error = (
        <div
          style={this.state.error ? {} : { display: "none" }}
          className="alert alert-danger container"
        >
          <p>Project: {this.state.project || "not defined"}</p>
          <p>Branch: {this.state.branch || "not defined"}</p>
          <p>Error: {this.state.error}</p>
        </div>
      );
    }
    return (
      <div className="App">
        <Header project={this.state.project} />
        {form}
        {error}
      </div>
    );
  }
}

export default App;
