import { userInfo, fetchFormData, triggerBuild } from ".";

const fakeUserInfo = {
  id: 1234,
  name: "Anakin Skywalker",
  username: "anakin.skywalker",
  email: "anakin.skywlaker@tatooine.com",
};

const fakePipelineId = {
  id: 1234,
  web_url: "path/to/pipeline",
};

beforeEach(() => {
  fetch.resetMocks();
});

describe("User api", () => {
  it("returns user info", async () => {
    fetch.mockResponseOnce(JSON.stringify(fakeUserInfo));
    const user = await userInfo();
    expect(user).toEqual(fakeUserInfo);
  });

  it("return error when user not found", () => {
    const errorResponse = new Response(null, {
      status: 404,
      statusText: "User Not Found",
    });
    fetch.mockReject(errorResponse);
    expect(userInfo()).rejects.toEqual(errorResponse);
  });
});

describe("Form api", () => {
  it("returns default form data when no .bod-ui.yml", () => {
    const errorMessage = "File Not Found";
    fetch.mockReject(
      new Response(null, { status: 404, statusText: errorMessage })
    );
    fetchFormData("project1", "branch1").catch((error) => {
      expect(error).toEqual({ status: 404, statusText: errorMessage });
    });
  });

  it("returns project form data", async () => {
    fetch.mockResponseOnce(
      `
    required: [target]
    properties:
      target:
          title: "Target to build"
          type: "string"
          default: "target1"
          enum: [target1, target2, target3, target4]
    `
    );
    const formData = await fetchFormData("project1", "branch1");
    expect(formData).toEqual({
      properties: {
        target: {
          default: "target1",
          enum: ["target1", "target2", "target3", "target4"],
          title: "Target to build",
          type: "string",
        },
      },
      required: ["target"],
    });
  });

  it("returns error on except branches", () => {
    fetch.mockResponseOnce(
      `
    except:
      - branch1
    `
    );
    expect(fetchFormData("project1", "branch1")).rejects.toEqual({
      status: 401,
      statusText: "Not allowed to trigger build on branch1 branch",
    });
  });

  it("returns form with other ui configuration file", async () => {
    fetch.mockResponseOnce(
      `
    required: [target]
    properties:
      target:
          title: "Target to build"
          type: "string"
          default: "target1"
          enum: [target1, target2, target3, target4]
    `
    );
    const formData = await fetchFormData(
      "project1",
      "branch1",
      "ui-config.yml"
    );
    expect(formData).toEqual({
      properties: {
        target: {
          default: "target1",
          enum: ["target1", "target2", "target3", "target4"],
          title: "Target to build",
          type: "string",
        },
      },
      required: ["target"],
    });
  });
});

describe("Trigger pipeline api", () => {
  it("returns pipeline reference", async () => {
    fetch.mockResponseOnce(JSON.stringify(fakePipelineId));
    const pipeline = await triggerBuild({
      project: "project1",
      branch: "branch1",
      variable1: "value1",
      variable2: "value2",
    });
    expect(pipeline).toEqual(fakePipelineId);
  });
  it("returns error if cannot trigger pipeline", () => {
    const errorMessage = "Project Not Found";
    fetch.mockReject(
      new Response(null, { status: 404, statusText: errorMessage })
    );
    triggerBuild({ project: "project1", branch: "branch1" }).catch((error) => {
      expect(error).toEqual({ status: 404, statusText: errorMessage });
    });
  });
});
