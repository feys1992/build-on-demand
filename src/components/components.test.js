import Header from "./header";
import LoaderSpinner from "./loaderSpinner";
import React from "react";

import { shallow } from "enzyme";

const serverUrl = process.env.REACT_APP_GITLAB_URL;

it("renders header div", () => {
  const componentUnderTest = shallow(<Header project="projectX" />);
  expect(
    componentUnderTest.find({ href: `${serverUrl}/projectX` })
  ).toHaveLength(1);
});

it("renders loader spinner div", () => {
  const componentUnderTest = shallow(
    <LoaderSpinner loadingMessage="Loading ..." />
  );
  expect(componentUnderTest.find("p").text()).toEqual("Loading ...");
});
