# build-on-demand

## Description

This project is a static web application which translate yaml ui form declared in _.bod-ui.yml_ to reactjs UI form.

It is meant to be used as a form generator to easily trigger a CI pipeline with custom variables.

The application is available at: https://grouperenault.gitlab.io/build-on-demand/ where user can choose the project and branch he wants to trigger pipelines.

Query parameters can be provided as follow:

https://grouperenault.gitlab.io/build-on-demand/?project=project_name&branch=branch_name
, where **project_name** and **branch_name** shall be provided by the user.

To render specific form, the project must contain ``.bod-ui.yml`` file which for example contains:

```yaml
required: ["target", "region", "debug"]
properties:
    target:
        title: "Target to build"
        type: "string"
        default: "target1"
        enum: [target1, target2, target3, target4]
    region:
        title: "Deployment region"
        type: "string"
        default: "eu-west1"
        enum: [eu-west1, us-west1, ap-south-1]
    debug:
        title: "debug mode"
        type: "boolean"
        default: false

```

Which will then translate to a form like:

![ui example](images/ui-form.png "UI example")

**Note:**
> * The form is available for all gitlab projects with following default fields: Project, Branch.
> * User can add new fields using the _.bod-ui.yml_ per projects/branches.


The UI description langage is defined by module https://react-jsonschema-form.readthedocs.io/en/latest/
which has a live playground here: https://rjsf-team.github.io/react-jsonschema-form/

``.bod-ui.yml`` is in fact a json-shema encoded in yml, with a ``ui`` field representing the UISchema as defined by react-jsonschema doc.

``ui`` field is optional and usually the json-schema is suffisant to generate usable UI.

## Architecture overview

```plantuml
skinparam sequence {
    DefaultFontName Helvetica
    ArrowColor black
    LifeLineBorderColor black

    ActorBackgroundColor #FC3
    ActorBorderColor black
    BoxBackgroundColor lightgrey
    BoxBorderColor black
    ParticipantBackgroundColor #FC3
    ParticipantBorderColor #FC3
}
actor User
box "Gitlab"
    participant BuildOnDemand as BOD
    participant "Gitlab API" as GL
end box

note over of BOD
    BOD is hosted on
    Gitlab Pages service.
end note
User -> BOD: get project/branch form
BOD -> GL
GL --> BOD: send .bod-ui.yml content
BOD -> BOD: extract ui form data
BOD --> User: display form to trigger ci pipeline

User -> User: define parameters
User -> BOD: trigger ci pipeline
BOD -> BOD: extract parameters values
BOD -> GL: trigger pipeline of the project
GL --> BOD
BOD --> User: redirect to project pipeline page
```

## How to deploy

The application has been developed to be easily deployed in a gitlab pages instance.

Users can deploy their own instance of the application in other gitlab group.

To do that :
* [Create an application token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token), with `api` scope and the redirect URI to _https://mygroup.gitlab.io/myproject/auth.html_. Copy the `Application ID`.
![Application Token](images/application_token.png)

* [Fork](https://gitlab.com/grouperenault/build-on-demand/-/forks/new) or copy the project in your gitlab group.

* In the project _Settings > CI/CD > Variables_, set the variable `BOD_CLIENT_ID` with `Application ID`.
![Project Variables](images/project_variable_setting.png)

* launch the pipeline and run manual '_pages_' job to deploy the application.
![Deploy Pages](images/deploy_pages.png)

## How to contribute

The frontend is based on ReactJS.

It is in charge of asking gitlab api to retrieve project/branch information to display the right UI Form to allow the user to trigger CI pipeline in gitlab.

Ensure node is installed.

```bash
make dev
```

To test

```bash
make ut
```

To build

```bash
make build
```

To start server for debugging

```bash
export CI_SERVER_URL=<gitlab server>
export CI_PAGES_URL=http://localhost:3000
export BOD_CLIENT_ID=<client id to perform authentication> # This id shall be created in gitlab application token menu (api permissions)
make serve
```

